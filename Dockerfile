# Start with Ubuntu 18.04 LTS.
FROM ubuntu:18.04

# Update package lists
RUN apt-get update && apt-get upgrade -y

# timezone
RUN apt-get install -yq tzdata && \
    ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

# install packages
RUN apt-get install -y \
    bash bc binfmt-support binutils bison build-essential bzip2 \
    ccache chrpath cpio curl device-tree-compiler diffstat \
    expect fakeroot file flex g++ g++-multilib gawk gcc \
    gcc-multilib genext2fs git git-core gnupg gperf gzip \
    lib32ncurses5-dev lib32z1-dev libc6-dev-i386 \
    libfile-which-perl libgl1-mesa-dev liblz4-tool libmpc3 \
    libssl-dev libx11-dev libxml2-utils live-build make \
    ncurses-dev patch patchelf perl pkg-config python python-pip \
    python-pyelftools qemu-user-static rsync sed ssh tar \
    texinfo time tree unzip wget x11proto-core-dev xsltproc zip \
    zlib1g-dev sqlite3 xxd

# install cmake
WORKDIR /usr
RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.22.1/cmake-3.22.1-linux-x86_64.tar.gz \
    | gunzip | tar --strip-components=1 -xf -

# install repo
RUN curl https://gitlab.com/anly8888/git-repo/-/raw/main/repo > /usr/bin/repo && \
    chmod a+rx /usr/bin/repo

# Clean
RUN apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN useradd -u 1000 anly

USER anly

WORKDIR /home/anly
RUN git config --global user.email "anly8888@gmail.com"
RUN git config --global user.name "Anly"
